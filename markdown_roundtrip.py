#!/usr/bin/env python3

from argparse import ArgumentParser
from contextlib import contextmanager
import logging
from os import environ
from pathlib import Path
import subprocess
import sys
from typing import Tuple


logger = logging.getLogger(__name__)


class RoundtripTester:
    LIB_NAME = "[unknown]"

    def __init__(self, input_path: Path):
        self.input_path = input_path

    def diff_roundtrip(self):
        roundtrip = self.do_roundtrip()
        subprocess.run(
            f'diff -u --color=always --label="input file" --label="after {self.LIB_NAME} roundtrip" "$orig_file" - | less -r',
            shell=True,
            env={"orig_file": str(self.input_path), **environ},
            input=roundtrip.encode(),
            stdout=sys.stdout, stderr=sys.stderr,
        )

    def text(self) -> str:
        return self.input_path.read_text()

    @contextmanager
    def textfile(self):
        with self.input_path.open("r") as file:
            yield file


class MarkdownItTester(RoundtripTester):
    LIB_NAME = "markdown-it & mdformat"

    def do_roundtrip(self) -> str:
        from markdown_it import MarkdownIt
        from mdformat.renderer import MDRenderer

        doc = MarkdownIt().parse(self.text())
        renderer = MDRenderer()
        return renderer.render(doc, {}, {})


# Roundtrip available since 1.1.0.
# Issue: <https://github.com/miyuchina/mistletoe/issues/4>
# PR: <https://github.com/miyuchina/mistletoe/pull/162>
class MistletoeTester(RoundtripTester):
    LIB_NAME = "mistletoe"

    def do_roundtrip(self) -> str:
        import mistletoe
        from mistletoe.markdown_renderer import MarkdownRenderer

        # Note: It's important to call MarkdownRenderer() _before_ Document(),
        # else all empty lines will be removed from the output.
        # This feels wrong, somehow. Should probably open an issue.
        with MarkdownRenderer() as renderer:
            doc = mistletoe.Document(self.text())
            return renderer.render(doc)


# Roundtrip available since 3.0.0.
class MistuneTester(RoundtripTester):
    LIB_NAME = "Mistune"

    def do_roundtrip(self) -> str:
        import mistune
        from mistune.renderers.markdown import MarkdownRenderer

        return mistune.create_markdown(
            renderer=MarkdownRenderer(),
        )(self.text())


class PandocTester(RoundtripTester):
    LIB_NAME = "pandoc"

    def do_roundtrip(self) -> str:
        import pandoc

        doc = pandoc.read(file=str(self.input_path), options=[
            "--wrap=preserve",
        ])
        return pandoc.write(doc)


TESTERS: Tuple[RoundtripTester] = (
    MarkdownItTester,
    MistletoeTester,
    MistuneTester,
    PandocTester,
)


def run() -> None:
    parser = ArgumentParser()
    parser.add_argument(
        "input_file",
        type=Path,
        help="the file to use for testing",
    )
    args = parser.parse_args()
    logging.basicConfig(level=logging.INFO)
    for testclass in TESTERS:
        logger.info(f"initializing tester for {testclass.LIB_NAME}")
        tester = testclass(args.input_file)
        logger.info(f"testing {testclass.LIB_NAME}")
        try:
            tester.diff_roundtrip()
        except ModuleNotFoundError:
            logger.exception(f"{testclass.LIB_NAME} seems unavailable")


if __name__ == "__main__":
    run()
